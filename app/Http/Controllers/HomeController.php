<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Author as AuthorModel;
use App\Models\Book as BookModel;
use App\Models\Genre as GenreModel;
use App\Models\BookToGenre as BookToGenreModel;

class HomeController extends Controller
{
    //
    /**
      * Look at the home URL
      *
      *
      * @return json
    **/
    public function index() {
      $data = ['api' => 'library',
              'status' => 'up',
              'description' => 'A custom library for your liking'];
      return json_encode($data);
    }

   /**
     * Create a new author instance.
     *
     * @param  Request  $request
     * @return json
   **/
    public function addAuthor(Request $request) {
      $data = [];
      try {
        $author = new AuthorModel();
        $author->firstname = $request->firstname;
        $author->lastname = $request->lastname;
        $author->birth_date = date('Y-m-d', strtotime($request->birthdate));
        $author->death_date = date('Y-m-d', strtotime($request->deathdate));
        $data['newauthor'] = $author->save();
      } catch( SQLException $e) {
        $data['newauthor'] = $e->getMessage();
      }
      echo json_encode($data);
      exit;
    }

    /**
      * Get a complete list of authors
      *
      * @param  Request  $request
      * @return json
    **/
    public function getAuthorList(Request $request) {
      $data = [];
      try {
        $data['authors'] = AuthorModel::all();
      } catch( SQLException $e) {
        $data['authors'] = $e->getMessage();
      }
      echo json_encode($data);
      exit;
    }

    /**
      * Get a one of authors
      *
      * @param  Request  $request
      * @return json
    **/
    public function getAuthor(Request $request) {
      $data = [];
      try {
        $data['author'] = AuthorModel::where('id', $request->id)->get();
      } catch( SQLException $e) {
        $data['author'] = $e->getMessage();
      }
      echo json_encode($data);
      exit;
    }

    /**
      * Update an author instance.
      *
      * @param  Request  $request
      * @param int $authorId
      * @return json
    **/
     public function updateAuthor(Request $request, $id) {
       $data = [];
       try {
         $author = new AuthorModel();
         $data['updatedauthor'] = $author->where('id', $id)
                                        ->update([
                                          'firstname' => $request->firstname,
                                          'lastname' => $request->lastname,
                                          'birth_date' => date('Y-m-d',
                                                  strtotime($request->birthdate)),
                                          'death_date' => date('Y-m-d',
                                                strtotime($request->deathdate))
                                        ]);
       } catch( SQLException $e) {
         $data['updatedauthor'] = $e->getMessage();
       }
       echo json_encode($data);
       exit;
     }

    /**
      * Create a new genre instance.
      *
      * @param  Request  $request
      * @return json
    **/
    public function addGenre(Request $request) {
      $data = [];
      try {
        $genre = new  GenreModel;
        $genre->name = $request->name;
        $genre->parent = $request->parent;
        $data['newgenre'] = $genre->save();
      } catch (SQLException $e) {
        $data['newgenre'] = $e->getMessage();
      }
      echo json_encode($data);
      exit;
    }

    /**
      * Get a whole genre list
      *
      * @param  Request  $request
      * @return json
    **/
    public function getGenreList() {
      $data = [];
      try {
        $data['genres'] = GenreModel::all();
      } catch( SQLException $e) {
        $data['genres'] = $e->getMessage();
      }
      echo json_encode($data);
      exit;
    }

    /**
      * Get a one of genres
      *
      * @param  Request  $request
      * @return json
    **/
    public function getgenre(Request $request) {
      $data = [];
      try {
        $data['genre'] = GenreModel::where('id', $request->id)->get();
      } catch( SQLException $e) {
        $data['genre'] = $e->getMessage();
      }
      echo json_encode($data);
      exit;
    }

    /**
      * Delete a genre.
      *
      * @param  Request  $request
      * @return json
    **/
    public function deleteGenre(Request $request) {
      $data = [];
      try {
        $genre = new GenreModel;
        $date['deletedGenre'] = $genre->where('id', $request->id)->delete();
      } catch(SQLException $e) {
        $date['deletedGenre'] = $e->getMessage();
      }
      echo json_encode($data);
      exit;
    }

    /**
      * Update a genre.
      *
      * @param  Request  $request
      * @param int $genreId
      * @return json
    **/
     public function updateGenre(Request $request, $id) {
       $data = [];
       try {
         $genre = new GenreModel();
         $data['updatedgenre'] = $genre->where('id', $id)
                                        ->update([
                                          'name' => $request->name
                                        ]);
       } catch( SQLException $e) {
         $data['updatedgenre'] = $e->getMessage();
       }
       echo json_encode($data);
       exit;
     }

    /**
      * Create a new book instance.
      *
      * @param  Request  $request
      * @return json
    **/
    public function addBook(Request $request) {
      $data = [];
      try {
        $book = new BookModel;
        $book->title = $request->title;
        $book->authorId = $request->author;
        $book->aparitionYear = $request->apparitionDate;
        $data['newbook'] = $book->save();
        foreach ($request->genres as $key => $value) {
          $data['newbookgenres'][] = DB::table('bookToGenre')->insert(
            ['bookId'=>$book->id, 'genreId'=>$value]
          );
        }
      } catch(SQLException $e) {
        $date['newbook'] = $e->getMessage();
      }
      echo json_encode($data);
      exit;
    }

    /**
      * Create a new book instance.
      *
      * @param  Request  $request
      * @return json
    **/
    public function deleteAuthor(Request $request) {
      $data = [];
      try {
        $author = new AuthorModel;
        $date['deletedauthor'] = $author->where('id', $request->id)->delete();
      } catch( SQLException $e) {
        $date['deletedauthor'] = $e->getMessage();
      }
      echo json_encode($data);
      exit;
    }

    /**
      * Get a whole book list
      *
      * @param  Request  $request
      * @return json
    **/
    public function getBookList() {
      $data = [];
      try {
        $books = new BookModel;
        $data['books'] = $books->leftjoin('author', 'author.id', '=', 'book.authorId')
                               ->leftJoin('bookToGenre', 'bookToGenre.bookId', '=', 'book.id')
                                ->leftJoin('genre', 'genre.id', '=', 'bookToGenre.genreId')
                                ->select('book.*', 'author.firstname', 'author.lastname', DB::raw('GROUP_CONCAT(`genre`.`name`, ", ") as genres'))
                                ->groupBy('book.id')
                                ->get();
      } catch( SQLException $e) {
        $data['books'] = $e->getMessage();
      }
      echo json_encode($data);
      exit;
    }

    /**
      * Delete a book.
      *
      * @param  Request  $request
      * @return json
    **/
    public function deleteBook(Request $request) {
      $data = [];
      try {
        $book = new BookModel;
        $date['deltedBook'] = $book->where('id', $request->id)->delete();
      } catch(SQLException $e) {
        $date['deltedBook'] = $e->getMessage();
      }
      echo json_encode($data);
      exit;
    }

    /**
      * Get a one of books
      *
      * @param  Request  $request
      * @return json
    **/
    public function getBook(Request $request) {
      $data = [];
      try {
        $data['book'] = BookModel::where('id', $request->id)->first();
        $data['bookgenres'] = $data['book']->genres()->get();
      } catch( SQLException $e) {
        $data['book'] = $e->getMessage();
      }
      echo json_encode($data);
      exit;
    }

    /**
      * Update a book.
      *
      * @param  Request  $request
      * @param int $bookId
      * @return json
    **/
     public function updateBook(Request $request, $id) {
       $data = [];
       try {
         $book = new BookModel();
         $data['updatedbook'] = $book->where('id', $id)
                                        ->update([
                                          'title' => $request->title,
                                          'authorId' => $request->author,
                                          'aparitionYear' => $request->apparitionDate
                                        ]);
         $bookToGenre = new BookToGenreModel();
         $bookToGenre->where('bookId', $id)->delete();
         $data['updatedbookgenres'] = [];
         foreach ($request->genres as $key => $value) {
           $data['updatedbookgenres'][] = DB::table('bookToGenre')->insert(
             ['bookId'=>$id, 'genreId'=>$value]
           );
         }
       } catch( SQLException $e) {
         $data['updatedbook'] = $e->getMessage();
       }
       echo json_encode($data);
       exit;
     }

   }
