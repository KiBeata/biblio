<?php

namespace App\Http\Resources;
use App\Models\Author as AuthorModel;
use Illuminate\Http\Resources\Json\JsonResource;

class Author extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    /**
      * Create a new author instance.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return json
    **/
     public function addauthor(Request $request) { 
       $data = [];
       $author = new AuthorModel();
       $author->firstname = $request->firstname;
       $author->lastname = $request->lastname;
       $author->date_birth = $request->date_birth;
       $author->date_death = $request->date_death;
       $data['newauthor'] = $author->save();
       return json_encode($data);
     }
}
