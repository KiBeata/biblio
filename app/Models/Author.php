<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
  const CREATED_AT = 'date_added';
  const UPDATED_AT = 'date_modified';

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  protected $table = 'author';
}
