<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
  const CREATED_AT = 'date_added';
  const UPDATED_AT = 'date_modified';

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  protected $table = 'book';


  /**
   * Get the book's genres.
   */
  public function genres()
  {
      return $this->belongsToMany('App\Models\Genre', 'bookToGenre', 'bookId', 'genreId');
  }

  /**
   * Get the book's author.
   */
  public function author()
  {
      return $this->morphOne('App\Models\Author', 'authorId');
  }


}
