<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookToGenre extends Model
{
    // table name
    protected $table = 'bookToGenre';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = ['bookId', 'genreId'];
}
