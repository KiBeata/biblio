<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
  const CREATED_AT = 'date_added';
  const UPDATED_AT = 'date_modified';

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  protected $table = 'genre';

  /**
   * Get the parent record associated with the genre.
   */
  public function parent()
  {
      return $this->hasOne('App\Models\Genre', 'parentId', 'id');
  }

  /**
   * The books that belong to the genre.
   */
  public function books()
  {
      return $this->belongsToMany('App\Models\Book');
  }


}
