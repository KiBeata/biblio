<?php

use Illuminate\Support\Facades\Route;
use App\Http\Resources;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

/*
|--------------------------------------------------------------------------
| Author Routes
|--------------------------------------------------------------------------
|
*/
Route::post('/api/author/add', 'HomeController@addAuthor');
Route::delete('/api/author/delete/{id}', 'HomeController@deleteAuthor');
Route::get('/api/author/list', 'HomeController@getAuthorList');
Route::get('/api/author/get/{id}', 'HomeController@getAuthor');
Route::post('/api/author/update/{id}', 'HomeController@updateAuthor');
/*
|--------------------------------------------------------------------------
| Genre Routes
|--------------------------------------------------------------------------
|
*/

Route::post('/api/genre/add', 'HomeController@addGenre');
Route::get('/api/genre/list', 'HomeController@getGenreList');
Route::delete('/api/genre/delete/{id}', 'HomeController@deleteGenre');
Route::get('/api/genre/get/{id}', 'HomeController@getGenre');
Route::post('/api/genre/update/{id}', 'HomeController@updateGenre');
/*
|--------------------------------------------------------------------------
| Book Routes
|--------------------------------------------------------------------------
|
*/
Route::post('/api/book/add', 'HomeController@addBook');
Route::get('/api/book/list', 'HomeController@getBookList');
Route::delete('/api/book/delete/{id}', 'HomeController@deleteBook');
Route::get('/api/book/get/{id}', 'HomeController@getBook');
Route::post('/api/book/update/{id}', 'HomeController@updateBook');
